package view.components;

import java.awt.*;
import javax.swing.*;

public class BorderPane extends JPanel {
	
	/* ************ CONSTRUCTOR ************* */
	// Sets the layout of the BorderPane JPanel to BorderLayout
	public BorderPane() {
        this.setLayout(new BorderLayout());
    }
}
